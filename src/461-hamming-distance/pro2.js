// Time Complexity O(1)
// Space Complexity O(1)

//ZOR it and convert the number to binary and count the number of 1s
//let z = x ^ y;
//let zToBin = z.toString(2)
//

export function hammingDistance(x, y) {
  let z = x ^ y;
  let sum = 0;

  while (z) {
    sum++;
    z &= z - 1;
  }

  return sum;
}
