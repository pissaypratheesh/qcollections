// HELP:
/**
 * 滑动窗口。需要替换数 = 窗口长度 - 窗口中最多的字符数
 * Sliding window. Number of replacements required = window length-maximum number of characters in window
 *
 * https://zhaonanli.gitbooks.io/leetcode/424.longest-repeating-character-replacement.html
 * https://www.geeksforgeeks.org/maximum-length-substring-having-all-same-characters-after-k-changes/
 */
var characterReplacement = function(s, K) {
  const count = new Map();
  let maxCount = 0; // 窗口中最多的那个字符的数量 The maximum number of characters in the window
  let [left, right] = [0, 0];
  let res = 0;

  while (right < s.length) {
    const newChar = s[right];
    count.set(newChar, (count.get(newChar) || 0) + 1);
    maxCount = Math.max(maxCount, count.get(newChar));
    ++right;
    if (right - left - maxCount <= K) {
      // 能替换所有非最多字符 Can replace all non-maximum characters
      res = Math.max(res, right - left);
    }

    while (right - left - maxCount > K && left <= right) {
      // 不能，则减少头字符直到删除了一个非最多字符 No, reduce the first character until a non-maximum character is deleted
      const oldChar = s[left++];
      count.set(oldChar, count.get(oldChar) - 1);
      // 重新找出最多字符 Find out the most characters
      maxCount = Array.from(count.values()).reduce((a, b) => Math.max(a, b), 0);
    }
  }

  return res;
};
