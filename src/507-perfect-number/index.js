/*
*https://leetcode.com/problems/perfect-number/solution/
* check till sqrt cz max num can b sqrt(n) *
* */
var l = console.log
//export
function checkPerfectNumber(num) {
  let i = 2;
  let sum = 1;
  let limit = Math.sqrt(num);

  while (i <= limit) {
    if (num % i === 0) {
      l(' those 2-->',i,num/i)
      sum += i + num / i;
    }
    i++;
  }

  return num > 2 && sum === num;
}

l(' fin-->',checkPerfectNumber(28))