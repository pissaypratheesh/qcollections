// HELP:
//https://leetcode.com/problems/elimination-game/?tab=Description
//http://xinglunote.blogspot.com/2017/04/leetcode-390-elimination-game.html
//http://xinglunote.blogspot.com/2017/04/leetcode-390-elimination-game.html
/**
 * @param {number} n
 * @return {number}
 */
var lastRemaining = function(n) {
  let left = true;
  let head = 1;
  let step = 1;

  while (n > 1) {
    if (left || n % 2 === 1) {
      head += step;
    }
    n = Math.floor(n / 2);
    step *= 2;
    left = !left;
  }

  return head;
};
