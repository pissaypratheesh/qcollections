# 8.Length Of Longest Substring

## Description

Given a string, find the length of the longest substring without repeating characters.

HAVE UNDERSTOOD THE PRO2 ONE, HAS VIDEO LINK
## Example

```javascript
Input: 'abcabcbb'
Output: 3
```

## From

[LeetCode](https://leetcode.com/articles/longest-substring-without-repeating-characters/)
