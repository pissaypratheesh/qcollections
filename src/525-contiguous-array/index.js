// HELP:

/*

This approach relies on the same premise as the previous approach.
    But, we need not use an array of size \text{2n+1}2n+1,
    since it isn't necessary that we'll encounter all the countcount values possible.
    Thus, we make use of a HashMap mapmap to store the entries in
   the form of (index, count)(index,count). We make an entry for a countcount in
   the mapmap whenever the countcount is encountered first, and later on use the
correspoding index to find the length of the largest subarray with equal no.
    of zeros and ones when the same countcount is encountered again.
*/

//https://leetcode.com/problems/contiguous-array/solution/
var l = console.log
//export
const findMaxLength = nums => {
  const map = new Map();
  map.set(0, -1);
  let max = 0;
  let count = 0;

  for (let i = 0; i < nums.length; i++) {
    count += nums[i] === 1 ? 1 : -1;
    l(' map-->',map,count,nums[i],map.has(count),i)
    if (map.has(count)) {
      max = Math.max(max, i - map.get(count));
    } else {
      map.set(count, i);
    }
  }

  return max;
};

l(findMaxLength([1,0,0]))