
// Best explanation: https://zhaonanli.gitbooks.io/leetcode/453.minimum-moves-to-equal-array-elements.html
/*
*
* Increasing 1 on all the remaining elements is equal to that subtracting 1 from one element.
initial_sum - final_sum = 1 * num_of_moves
Because we only can do subtracting, and we want to minimize the number of moves,
* so the final equal elements should be the min value of the current array. So the final_sum = min(arr) * len(arr).
* */


export const minMoves = nums => {
  const min = Math.min(...nums);
  const sum = nums.reduce((acc, n) => acc + n, 0);
  return sum - min * nums.length;
};
