// HELP:
//http://anothercasualcoder.blogspot.com/2016/12/leetcode-combination-sum-iv.html
let l = console.log
//export
function combinationSum4(nums, target) {
  const dp = Array(target + 1).fill(0);
  dp[0] = 1;
  for (let i = 1; i <= target; i++) {
    nums.forEach(n => {
      if (n <= i)
        dp[i] += dp[i - n];
    });
  }
  l(dp)
  return dp[target];
}
console.log("--.",combinationSum4([1, 2, 3],4))