//https://stackoverflow.com/questions/9960908/permutations-in-javascript
//https://www.30secondsofcode.org/js/s/permutations
/*

const permutations = arr => {
    if (arr.length <= 2) return arr.length === 2 ? [arr, [arr[1], arr[0]]] : arr;
    return arr.reduce(
        (acc, item, i) =>{
            console.log("acc,item,i-->",item,i, permutations([...arr.slice(0, i), ...arr.slice(i + 1)]),'     from ',arr,'     to get:',permutations([...arr.slice(0, i), ...arr.slice(i + 1)]).map(val => [item, ...val]))
            return acc.concat(
                permutations([...arr.slice(0, i), ...arr.slice(i + 1)]).map(val => [item, ...val])
            )
        },
        []
    );
};
EXAMPLES
permutations([1, 33, 5])
*/



/**
 * @param {number} n
 * @param {number} k
 * @return {string}
 */
var getPermutation = function(n, k) {
  const arr = Array.from({ length: n }, (_, index) => index + 1);
  let res = '';
  let count = 0;
  backtrack(arr, '');
  return res;

  function backtrack(arr, temp) {
    if (count > k) return;

    if (temp.length === n) {
      count++;
      if (count === k) {
        res = temp;
      }
    } else {
      for (let i = 0; i < arr.length; i++) {
        const copy = [...arr];
        const current = arr[i];
        copy.splice(i, 1);
        backtrack(copy, temp + current);
      }
    }
  }
};

console.log(getPermutation(3, 3));
