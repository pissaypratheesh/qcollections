export function repeatedSubstringPattern(s) {
  let subString = '';
  let i = 0;

  while (i < s.length) {
    const len = subString.length;
    // is the next substr starting? && is the substr len multiple of str len? && next char to substr len is the next repeating substr???
    if (subString[0] === s[i] && !(s.length % len) && subString === s.slice(i, i + len)) {
      i += len;
      //is it the end of the match??
      if (i === s.length)
        return true;
    } else {
      subString = s.slice(0, i + 1);
      i++;
    }
  }

  return false;
}
