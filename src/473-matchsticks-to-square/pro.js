// HELP:
//Explanation: https://massivealgorithms.blogspot.com/2016/12/leetcode-473-matchsticks-to-square.html

let l = console.log
//export
const makeSquare = nums => {
  if (nums.length === 0) return false;

  const total = nums.reduce((acc, x) => acc + x, 0);
  const sideLength = total / 4;
  let res = false;
  let hash = [];
  dfs(0, 0, 0);
  l('hash-->',hash)
  return res;

  function dfs(pos, sum, count) {
    if (res) return;
    if (count === 3) {
      res = true;
      return;
    }

    if (sum === sideLength) {
      l('fopiund the sum-->',pos,sum,count)
      dfs(0, 0, count + 1);
    }

    for (let i = pos; i < nums.length; i++) {
      if (hash[i] || sum + nums[i] > sideLength) continue;

      hash[i] = true;
      dfs(i + 1, sum + nums[i], count);
      hash[i] = false;
    }
  }
};


l(makeSquare([3,3,3,3,4]))
//l(makeSquare([1,1,2,2,2]))