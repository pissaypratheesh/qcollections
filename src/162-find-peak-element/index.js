//Best: https://www.youtube.com/watch?time_continue=7&v=a7D77DdhlFc&feature=emb_logo

export function findPeakElement(nums) {
  let max = -Infinity;
  let position = 0;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] > max) {
      position = i;
      max = nums[i];
    }
  }

  return position;
}
