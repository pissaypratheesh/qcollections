


// HELP:

/*
* Best:
* let subsets = nums => {
    l("\n numbssss-->",nums)
    return nums.reduce((sets, n) => {
        l("\n n,sets-->",n,sets,
            '\n propsecces',...sets.map(set => [...set, n]),
            '\nwats returning-->',[...sets, ...sets.map(set => [...set, n])])

        return [...sets, ...sets.map(set => [n,...set])]
    }, [[]]);
}
* */


export function subsets(nums) {
  let res = [];
  backtracking([], nums, 0);

  function backtracking(curr, remaining, start) {
    res.push(curr);

    for (let i = start; i < remaining.length; i++) {
      backtracking(
        [...curr, remaining[i]],
        [...remaining.slice(0, i), ...remaining.slice(i + 1)],
        start,
      );

      start++;
    }
  }

  return res;
}
