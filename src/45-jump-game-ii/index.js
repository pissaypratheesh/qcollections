// HELP:
//https://www.youtube.com/watch?v=vBdo7wtwlXs
export function jump(nums) {
  let jumps = 0;
  let curEnd = 0;
  let curFarthest = 0; //Length of the longest ladder so far

  for (let i = 0; i < nums.length - 1; i++) {
    curFarthest = Math.max(i + nums[i], curFarthest);
    if (i === curEnd) {
      jumps++;
      curEnd = curFarthest;
    }
  }

  return jumps;
}
