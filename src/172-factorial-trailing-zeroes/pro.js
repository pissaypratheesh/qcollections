// HELP:
//https://www.geeksforgeeks.org/count-trailing-zeroes-factorial-number/
/*
*
* Trailing 0s in n! = Count of 5s in prime factors of n!
                  = floor(n/5) + floor(n/25) + floor(n/125) + ....
*
* */

/*
*
// C++ program to count
// trailing 0s in n!
#include <iostream>
using namespace std;

// Function to return trailing
// 0s in factorial of n
int findTrailingZeros(int n)
{
    // Initialize result
    int count = 0;

    // Keep dividing n by powers of
    // 5 and update count
    for (int i = 5; n / i >= 1; i *= 5)
        count += n / i;

    return count;
}
* */

export function trailingZeroes(n) {
  let res = 0;
  let f = 1;

  while (f * 5 <= n) {
    f *= 5;
    res += parseInt(n / f);
  }

  return res;
}
