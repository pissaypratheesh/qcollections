export function isPrime(num) {
  if (num < 2) return false;
  let divisor = 2;
  const maxDivisor = Math.sqrt(num);

  while (divisor <= maxDivisor) {
    if (num % divisor === 0) return false;
    divisor++;
  }

  return true;
}

/*

Best:
    function isPrime(n) {
      var divisor = 3,
          limit = Math.sqrt(n);

      //check simple cases
      if (n == 2 || n == 3)
        return true;
      if (n % 2 == 0)
        return false;

      while (divisor <= limit)
      {
        if (n % divisor == 0)
          return false;
        else
          divisor += 2;
      }
      return true;
    }

*/
